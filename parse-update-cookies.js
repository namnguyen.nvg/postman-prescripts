function getCookies(resCookies) {
    var cookiesStr = '';
    for (var index in resCookies) {
        var cookies = resCookies[index];
        if (isRealValue(cookies) && index === "members") {
            for (var index2 in cookies) {
                var object = cookies[index2];
                cookiesStr += object.name + '=' + decodeURIComponent(object.value) + ';';
            }
        }
    }
    return cookiesStr;
}

function getCookiesMap(cookiesString) {
  return cookiesString.split(";")
    .map(function(cookieString) {
        return cookieString.trim().split("=");
    })
    .reduce(function(acc, curr) {
        acc[curr[0]] = curr[1];
        return acc;
    }, {});
}

function updateCookies(resCookies, cookiesMap) {
    for (var index in resCookies) {
        var cookies = resCookies[index];
        if (isRealValue(cookies) && index === "members") {
            for (var index2 in cookies) {
                var object = cookies[index2];
                cookiesMap[object.name] = decodeURIComponent(object.value);
            }
        }
    }
    return cookiesMap;
}

function mapToString(dictionary) {
    var str = '';
    for (const [ key, value ] of Object.entries(dictionary)) {
        if (isRealValue(value)) {
            str += key + '=' + value + ';';
        }
    }
    return str;
}

function isRealValue(obj) {
    return obj && obj !== 'null' && obj !== 'undefined';
}